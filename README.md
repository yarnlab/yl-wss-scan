# Bitbucket Pipelines Pipe: WhiteSource

We help you harness the power of open source without compromising on security or agility.
WhiteSource seamlessly integrates with your build (and all other development environments) to automatically detect all open source components in your products. It then provides all needed information about your open source like security vulnerabilities, licenses, versioning and more. It enables you to enforce policies automatically, get real-time alerts and generates a wide range of reports.  WhiteSource supports over 200 programming languages and all environments.
([see the supported languages](https://www.whitesourcesoftware.com/whitesource-languages/))

## YAML Definition

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:           
    
```yaml
- pipe: WhitesourceSoftware/whitesource-scan:1.3.0
  variables:
    # API_KEY: '<string>'             # Optional
    # DIRECTORY: '<string>'           # Optional
    # CONFIG_FILE_PATH: '<string>'    # Optional
    # COMMANDS_FILE_PATH: '<string>'  # Optional
```

## Variables

| Variable                    | Usage | Required | Default |
| --------------------------- | ----- | -------- | ------- |
| API_KEY                     | WhiteSource Organization Account Api Key | false | ApiKey found in config file      |
| DIRECTORY                   | Project to Scan Directory. Can contain multiple directories separated by commas | false | '.' |
| CONFIG_FILE_PATH            | Configuration File Relative Path         | false |  './wss-unified-agent.config' |
| COMMANDS_FILE_PATH            | Path to the required bash commands in case any customization is required in the generic orb. Add packages to update and install, environemnt variables etc.         | false |  './install-commands.sh' |


_Variables are optional, If the value is not supplied then the WhiteSource Scanner uses the default value._

## Details

The WhiteSource Unified-Agent configuration file should be downloaded to a project. The configuration file path including the file name should be set in the 'config file path' variable.
More details for configuration file parameters can be found in [Unified Agent Configuration File & Parameters](https://whitesource.atlassian.net/wiki/spaces/WD/pages/489160834/Unified+Agent+Configuration+File+Parameters) page.

---
You have the option to view the logs, and then navigate to the WhiteSource GUI. The URL for the scan result link is indicated in the logs.

![Whitesource Log](https://bitbucket.org/WhitesourceSoftware/whitesource-scan/raw/master/images/Whitesource_Log.gif)

You can view the compliance and security data for the project that was scanned on WhiteSource GUI (Web interface)

![Whitesource Vulnerabilities](https://bitbucket.org/WhitesourceSoftware/whitesource-scan/raw/master/images/Whitesource_Vulnerabilities.gif)

Starting with whitesource-scan version 1.3.0, if the Unified Agent fails inside the pipeline (meaning the Unified Agent's exit code is not 0), we fail the pipe with the same exit code of the Unified Agent.
 
## Prerequisites
* Active WhiteSource account with access to the GUI, and permissions to run WhiteSource Unified Agent.
* Active Bitbucket cloud account with one or more repositories.


## Examples

### Basic example:

```yaml
script:
  - pipe: WhitesourceSoftware/whitesource-scan:1.3.0
```

### Advanced example: 
    
```yaml
script:
  - pipe: WhitesourceSoftware/whitesource-scan:1.3.0
    variables:
        API_KEY: $API_KEY
        DIRECTORY: '.'
        CONFIG_FILE_PATH: './someFolder/wss-unified-agent.config'
        COMMANDS_FILE_PATH: './someFolder/install-commands.sh'
```
    
## Support
If you'd like help with this pipe, or you have an issue or feature request, 
Then please contact WhiteSource support at support@whitesourcesoftware.com.

If you're reporting an issue, please include:

* the version of the pipe
* relevant logs and error messages
* steps to reproduce

## License
Copyright (c) 2018 Whitesource Software.
See [LICENSE.txt](LICENSE.txt), [LICENSE-3RD-PARTY.txt](LICENSE-3RD-PARTY.txt) and [NOTICE-3RD-PARTY.txt](NOTICE-3RD-PARTY.txt) 

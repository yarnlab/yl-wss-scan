#!/bin/bash

echo ""

COMMANDS_FILE_PATH=${COMMANDS_FILE_PATH:="./install-commands.sh"}

echo "*******************************************************************************************************************"
echo "                          Start Executing Commands File"
echo "*******************************************************************************************************************"
echo ""

if [[ -f ${COMMANDS_FILE_PATH} ]]
then
    echo "Executing file: ${COMMANDS_FILE_PATH}"
    echo ""
    bash ${COMMANDS_FILE_PATH}
else
    echo "Couldn't find file ${COMMANDS_FILE_PATH}"
	echo "Skip executing commands file."
fi

echo ""
echo "*******************************************************************************************************************"
echo "                          Finish Executing Commands File"
echo "*******************************************************************************************************************"


echo ""
echo ""
echo "*******************************************************************************************************************"
echo "				Start Running WhiteSource Unified Agent       "
echo "*******************************************************************************************************************"
echo ""

DIRECTORY=${DIRECTORY:="."}
API_KEY=${API_KEY}
CONFIG_FILE_PATH=${CONFIG_FILE_PATH:="./wss-unified-agent.config"}
#changed here to allow insertion of java heap commands
if [[ -z "${API_KEY}" ]]; then
    curl -LJO https://github.com/whitesource/unified-agent-distribution/releases/latest/download/wss-unified-agent.jar
    java -jar -Xms512m -Xmx2048m wss-unified-agent.jar -c "${CONFIG_FILE_PATH}" -appPath "${DIRECTORY}"/package.json" -d "${DIRECTORY}"
else
    curl -LJO https://github.com/whitesource/unified-agent-distribution/releases/latest/download/wss-unified-agent.jar
    java -jar -Xms512m -Xmx2048m wss-unified-agent.jar -apiKey "${API_KEY}" -c "${CONFIG_FILE_PATH}" -appPath "${DIRECTORY}"/package.json" -d "${DIRECTORY}"
fi
# Save the exit code of UA into "ua_exit_code" variable
ua_exit_code=$?
echo ""
echo "*******************************************************************************************************************"
echo "				Finish Running WhiteSource Unified Agent       "
echo "*******************************************************************************************************************"
exit $ua_exit_code

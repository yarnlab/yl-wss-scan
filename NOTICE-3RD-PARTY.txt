

--- beginning of NOTICE ---
Apache Ant
Copyright 1999-2014 The Apache Software Foundation

This product includes software developed at
The Apache Software Foundation (http://www.apache.org/).

The <sync> task is based on code Copyright (c) 2002, Landmark
Graphics Corp that has been kindly donated to the Apache Software
Foundation.

--- end of NOTICE ---

------------------------------------------------------------------------------- 

--- beginning of NOTICE ---

JCommander Copyright Notices 
============================

Copyright 2010 Cedric Beust <cedric@beust.com>

--- end of NOTICE ---
------------------------------------------------------------------------------- 

--- beginning of NOTICE ---


Snappy Copyright Notices 
=========================

* Copyright 2011 Dain Sundstrom <dain@iq80.com>
* Copyright 2011, Google Inc.<opensource@google.com>


Snappy License
===============
Copyright 2011, Google Inc.
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:

    * Redistributions of source code must retain the above copyright
notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
copyright notice, this list of conditions and the following disclaimer
in the documentation and/or other materials provided with the
distribution.
    * Neither the name of Google Inc. nor the names of its
contributors may be used to endorse or promote products derived from
this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

--- end of NOTICE ---

-------------------------------------------------------------------------------


--- beginning of NOTICE ---

Apache Maven
Copyright 2001-2015 The Apache Software Foundation

This product includes software developed at
The Apache Software Foundation (http://www.apache.org/).

--- end of NOTICE ---

-------------------------------------------------------------------------------


--- beginning of NOTICE ---


                            The Netty Project
                            =================

Please visit the Netty web site for more information:

  * http://netty.io/

Copyright 2014 The Netty Project

The Netty Project licenses this file to you under the Apache License,
version 2.0 (the "License"); you may not use this file except in compliance
with the License. You may obtain a copy of the License at:

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
License for the specific language governing permissions and limitations
under the License.

Also, please refer to each LICENSE.<component>.txt file, which is located in
the 'license' directory of the distribution file, for the license terms of the
components that this product depends on.

-------------------------------------------------------------------------------
This product contains the extensions to Java Collections Framework which has
been derived from the works by JSR-166 EG, Doug Lea, and Jason T. Greene:

  * LICENSE:
    * license/LICENSE.jsr166y.txt (Public Domain)
  * HOMEPAGE:
    * http://gee.cs.oswego.edu/cgi-bin/viewcvs.cgi/jsr166/
    * http://viewvc.jboss.org/cgi-bin/viewvc.cgi/jbosscache/experimental/jsr166/

This product contains a modified version of Robert Harder's Public Domain
Base64 Encoder and Decoder, which can be obtained at:

  * LICENSE:
    * license/LICENSE.base64.txt (Public Domain)
  * HOMEPAGE:
    * http://iharder.sourceforge.net/current/java/base64/

This product contains a modified portion of 'Webbit', an event based  
WebSocket and HTTP server, which can be obtained at:

  * LICENSE:
    * license/LICENSE.webbit.txt (BSD License)
  * HOMEPAGE:
    * https://github.com/joewalnes/webbit

This product contains a modified portion of 'SLF4J', a simple logging
facade for Java, which can be obtained at:

  * LICENSE:
    * license/LICENSE.slf4j.txt (MIT License)
  * HOMEPAGE:
    * http://www.slf4j.org/

This product contains a modified portion of 'Apache Harmony', an open source
Java SE, which can be obtained at:

  * LICENSE:
    * license/LICENSE.harmony.txt (Apache License 2.0)
  * HOMEPAGE:
    * http://archive.apache.org/dist/harmony/

This product contains a modified portion of 'jbzip2', a Java bzip2 compression
and decompression library written by Matthew J. Francis. It can be obtained at:

  * LICENSE:
    * license/LICENSE.jbzip2.txt (MIT License)
  * HOMEPAGE:
    * https://code.google.com/p/jbzip2/

This product contains a modified portion of 'libdivsufsort', a C API library to construct
the suffix array and the Burrows-Wheeler transformed string for any input string of
a constant-size alphabet written by Yuta Mori. It can be obtained at:

  * LICENSE:
    * license/LICENSE.libdivsufsort.txt (MIT License)
  * HOMEPAGE:
    * https://github.com/y-256/libdivsufsort

This product contains a modified portion of Nitsan Wakart's 'JCTools', Java Concurrency Tools for the JVM,
 which can be obtained at:

  * LICENSE:
    * license/LICENSE.jctools.txt (ASL2 License)
  * HOMEPAGE:
    * https://github.com/JCTools/JCTools

This product optionally depends on 'JZlib', a re-implementation of zlib in
pure Java, which can be obtained at:

  * LICENSE:
    * license/LICENSE.jzlib.txt (BSD style License)
  * HOMEPAGE:
    * http://www.jcraft.com/jzlib/

This product optionally depends on 'Compress-LZF', a Java library for encoding and
decoding data in LZF format, written by Tatu Saloranta. It can be obtained at:

  * LICENSE:
    * license/LICENSE.compress-lzf.txt (Apache License 2.0)
  * HOMEPAGE:
    * https://github.com/ning/compress

This product optionally depends on 'lz4', a LZ4 Java compression
and decompression library written by Adrien Grand. It can be obtained at:

  * LICENSE:
    * license/LICENSE.lz4.txt (Apache License 2.0)
  * HOMEPAGE:
    * https://github.com/jpountz/lz4-java

This product optionally depends on 'lzma-java', a LZMA Java compression
and decompression library, which can be obtained at:

  * LICENSE:
    * license/LICENSE.lzma-java.txt (Apache License 2.0)
  * HOMEPAGE:
    * https://github.com/jponge/lzma-java

This product contains a modified portion of 'jfastlz', a Java port of FastLZ compression
and decompression library written by William Kinney. It can be obtained at:

  * LICENSE:
    * license/LICENSE.jfastlz.txt (MIT License)
  * HOMEPAGE:
    * https://code.google.com/p/jfastlz/

This product contains a modified portion of and optionally depends on 'Protocol Buffers', Google's data
interchange format, which can be obtained at:

  * LICENSE:
    * license/LICENSE.protobuf.txt (New BSD License)
  * HOMEPAGE:
    * https://github.com/google/protobuf

This product optionally depends on 'Bouncy Castle Crypto APIs' to generate
a temporary self-signed X.509 certificate when the JVM does not provide the
equivalent functionality.  It can be obtained at:

  * LICENSE:
    * license/LICENSE.bouncycastle.txt (MIT License)
  * HOMEPAGE:
    * http://www.bouncycastle.org/

This product optionally depends on 'Snappy', a compression library produced
by Google Inc, which can be obtained at:

  * LICENSE:
    * license/LICENSE.snappy.txt (New BSD License)
  * HOMEPAGE:
    * https://github.com/google/snappy

This product optionally depends on 'JBoss Marshalling', an alternative Java
serialization API, which can be obtained at:

  * LICENSE:
    * license/LICENSE.jboss-marshalling.txt (GNU LGPL 2.1)
  * HOMEPAGE:
    * http://www.jboss.org/jbossmarshalling

This product optionally depends on 'Caliper', Google's micro-
benchmarking framework, which can be obtained at:

  * LICENSE:
    * license/LICENSE.caliper.txt (Apache License 2.0)
  * HOMEPAGE:
    * https://github.com/google/caliper

This product optionally depends on 'Apache Commons Logging', a logging
framework, which can be obtained at:

  * LICENSE:
    * license/LICENSE.commons-logging.txt (Apache License 2.0)
  * HOMEPAGE:
    * http://commons.apache.org/logging/

This product optionally depends on 'Apache Log4J', a logging framework, which
can be obtained at:

  * LICENSE:
    * license/LICENSE.log4j.txt (Apache License 2.0)
  * HOMEPAGE:
    * http://logging.apache.org/log4j/

This product optionally depends on 'Aalto XML', an ultra-high performance
non-blocking XML processor, which can be obtained at:

  * LICENSE:
    * license/LICENSE.aalto-xml.txt (Apache License 2.0)
  * HOMEPAGE:
    * http://wiki.fasterxml.com/AaltoHome

This product contains a modified version of 'HPACK', a Java implementation of
the HTTP/2 HPACK algorithm written by Twitter. It can be obtained at:

  * LICENSE:
    * license/LICENSE.hpack.txt (Apache License 2.0)
  * HOMEPAGE:
    * https://github.com/twitter/hpack

This product contains a modified portion of 'Apache Commons Lang', a Java library
provides utilities for the java.lang API, which can be obtained at:

  * LICENSE:
    * license/LICENSE.commons-lang.txt (Apache License 2.0)
  * HOMEPAGE:
    * https://commons.apache.org/proper/commons-lang/

--- end of NOTICE ---

-------------------------------------------------------------------------------


--- beginning of NOTICE ---

Apache Commons Codec
Copyright 2002-2014 The Apache Software Foundation

This product includes software developed at
The Apache Software Foundation (http://www.apache.org/).

src/test/org/apache/commons/codec/language/DoubleMetaphoneTest.java
contains test data from http://aspell.net/test/orig/batch0.tab.
Copyright (C) 2002 Kevin Atkinson (kevina@gnu.org)

===============================================================================

The content of package org.apache.commons.codec.language.bm has been translated
from the original php source code available at http://stevemorse.org/phoneticinfo.htm
with permission from the original authors.
Original source copyright:
Copyright (c) 2008 Alexander Beider & Stephen P. Morse.

--- end of NOTICE ---

-------------------------------------------------------------------------------


--- beginning of NOTICE ---

Apache Commons Collections
Copyright 2001-2008 The Apache Software Foundation

This product includes software developed by
The Apache Software Foundation (http://www.apache.org/).

--- end of NOTICE ---

-------------------------------------------------------------------------------


--- beginning of NOTICE ---

Apache Commons Compress
Copyright 2002-2014 The Apache Software Foundation

This product includes software developed at
The Apache Software Foundation (http://www.apache.org/).

The files in the package org.apache.commons.compress.archivers.sevenz
were derived from the LZMA SDK, version 9.20 (C/ and CPP/7zip/),
which has been placed in the public domain:

"LZMA SDK is placed in the public domain." (http://www.7-zip.org/sdk.html)

--- end of NOTICE ---

-------------------------------------------------------------------------------


--- beginning of NOTICE ---

Apache Commons IO
Copyright 2002-2012 The Apache Software Foundation

This product includes software developed by
The Apache Software Foundation (http://www.apache.org/).

--- end of NOTICE ---

-------------------------------------------------------------------------------


--- beginning of NOTICE ---
Apache Commons Lang
Copyright 2001-2008 The Apache Software Foundation

This product includes software developed by
The Apache Software Foundation (http://www.apache.org/).

--- end of NOTICE ---

-------------------------------------------------------------------------------


--- beginning of NOTICE ---

Apache Commons Lang
Copyright 2001-2015 The Apache Software Foundation

This product includes software developed at
The Apache Software Foundation (http://www.apache.org/).

This product includes software from the Spring Framework,
under the Apache License 2.0 (see: StringUtils.containsWhitespace())

--- end of NOTICE ---

-------------------------------------------------------------------------------


--- beginning of NOTICE ---

Apache Commons Logging
Copyright 2003-2014 The Apache Software Foundation

This product includes software developed at
The Apache Software Foundation (http://www.apache.org/).

--- end of NOTICE ---

-------------------------------------------------------------------------------


--- beginning of NOTICE ---

This product includes software developed by
The Apache Software Foundation (http://www.apache.org/).

--- end of NOTICE ---

-------------------------------------------------------------------------------


--- beginning of NOTICE ---

Apache Commons VFS
Copyright 2002-2017 The Apache Software Foundation

This product includes software developed at
The Apache Software Foundation (http://www.apache.org/).

--- end of NOTICE ---

-------------------------------------------------------------------------------


--- beginning of NOTICE ---

Apache HttpComponents Client
Copyright 1999-2015 The Apache Software Foundation

This product includes software developed at
The Apache Software Foundation (http://www.apache.org/).

--- end of NOTICE ---

-------------------------------------------------------------------------------


--- beginning of NOTICE ---
Apache HttpComponents Core
Copyright 2005-2014 The Apache Software Foundation

This product includes software developed at
The Apache Software Foundation (http://www.apache.org/).

This project contains annotations derived from JCIP-ANNOTATIONS
Copyright (c) 2005 Brian Goetz and Tim Peierls. See http://www.jcip.net

--- end of NOTICE ---

-------------------------------------------------------------------------------

--- beginning of NOTICE ---

# Jackson JSON processor

Jackson is a high-performance, Free/Open Source JSON processing library.
It was originally written by Tatu Saloranta (tatu.saloranta@iki.fi), and has
been in development since 2007.
It is currently developed by a community of developers, as well as supported
commercially by FasterXML.com.

## Licensing

Jackson core and extension components may licensed under different licenses.
To find the details that apply to this artifact see the accompanying LICENSE file.
For more information, including possible other licensing options, contact
FasterXML.com (http://fasterxml.com).

## Credits

A list of contributors may be found from CREDITS file, which is included
in some artifacts (usually source distributions); but is always available
from the source code management (SCM) system project uses.

--- end of NOTICE ---

-------------------------------------------------------------------------------

--- beginning of NOTICE ---

This product includes software developed by
The Apache Software Foundation (http://www.apache.org/).

--- end of NOTICE ---

-------------------------------------------------------------------------------

--- beginning of NOTICE ---

Spring Framework ${version}
Copyright (c) 2002-${copyright} Pivotal, Inc.

This product is licensed to you under the Apache License, Version 2.0
(the "License"). You may not use this product except in compliance with
the License.

This product may include a number of subcomponents with separate
copyright notices and license terms. Your use of the source code for
these subcomponents is subject to the terms and conditions of the
subcomponent's license, as noted in the license.txt file.

--- end of NOTICE ---

-------------------------------------------------------------------------------

--- beginning of NOTICE ---

Apache Velocity

Copyright (C) 2000-2007 The Apache Software Foundation

This product includes software developed at
The Apache Software Foundation (http://www.apache.org/).

--- end of NOTICE ---

-------------------------------------------------------------------------------

--- beginning of NOTICE ---

Apache XBean
Copyright 2005-20010 The Apache Software Foundation

This product includes software developed at
The Apache Software Foundation (http://www.apache.org/).

--- end of NOTICE ---

-------------------------------------------------------------------------------

--- beginning of NOTICE ---

Rhino is licensed subject to the terms of the Mozilla Public License, v. 2.0.
See "License.txt" for the text of the license.

Rhino contains the following additional software:

----

Portions of the floating-point conversion code, and portions of the test suite
come from the Google V8 JavaScript engine and are subject to the following:

Copyright 2010-2015 the V8 project authors. All rights reserved.
Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:

    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of Google Inc. nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

--- end of NOTICE ---







